; TODO list of unimplemented funcitons
; void _seek u64 fd u64 whither
; void _advance u64 fd u64 offset
; u64 _rand_seed void
; void _remove raw u8 filename u64 size
; void _mkdir raw u8 dirname u64 size u8 mode
; void _rename raw u8 oldname raw u8 newname u64 oldsize u64 newsize

section .text
global _pwrite
global _write
global _read
global _open
global _close

_pwrite:mov rdx, rsi
	mov rsi, rdi
	mov rdi, 2
	mov rax, 1
	syscall
	ret

_write:	mov rax, 1
	syscall
	ret

_read:	xor rax, rax
	syscall
	ret

_open:	mov rsi, rdx
	mov rdx, rcx
	mov rax, 2
	syscall
	ret

_close:	mov rax, 3
	syscall
	ret
