#include <stdint.h>
#include "arch.h"

struct string {
	uint64_t size;
	char *data;
};

void print(struct string *s) {
	_pwrite(s->data, s->size);
}

void println(struct string *s0) {
	char s[s0->size + 2];
	for (int i = 0; i < s0->size; i++) {
		s[i] = s0->data[i];
	}
	s[s0->size + 0] = '\n';
	s[s0->size + 1] = 0;
	_pwrite(s, s0->size + 2);
}

void eprint(struct string *s) {
	_ewrite(s->data, s->size);
}

void eprintln(struct string *s0) {
	char s[s0->size + 2];
	for (int i = 0; i < s0->size; i++) {
		s[i] = s0->data[i];
	}
	s[s0->size + 0] = '\n';
	s[s0->size + 1] = 0;
	_ewrite(s, s0->size + 2);
}

void write(uint64_t fd; struct string *s) {
	_write(fd, s->data, s->size);
}

void writeln(uint64_t fd; struct string *s0) {
	char s[s0->size + 2];
	for (int i = 0; i < s0->size; i++) {
		s[i] = s0->data[i];
	}
	s[s0->size + 0] = '\n';
	s[s0->size + 1] = 0;
	_write(fd, s, s0->size + 2);
}
